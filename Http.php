<?php

trait Http {
  public function http($method, $url, $headers=[], $data=null) {
    $method = strtoupper($method);
    echo "$method $url\n";

    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
    if ($headers) {
      print_r($headers);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    }
    if ($data) {
      echo  "$data\n";
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $result = curl_exec($ch);

    curl_close($ch);
    return $result;
  }
}
