<?php
require_once('Http.php');

class LyftAPI {
  use Http;

  private static $baseUrl = 'https://api.lyft.com';
  private static $tokenRefreshOffset = 600;
  private $clientId;
  private $clientSecret;
  private $clientToken;
  private $userToken;

  public function __construct(
    $clientId, $clientSecret, $authCode=null, $userToken=null) {
    $this->clientId = $clientId;
    $this->clientSecret = $clientSecret;
    $this->clientToken = $this->getClientToken();
    if ($authCode) {
      $this->userToken = $this->getUserToken($authCode);
    } else if ($userToken) {
      $this->userToken = $userToken;
    }
  }

  public function getAuthHeader($endpoint) {
    $token = null;
    $this->clientToken = $this->getClientToken();
    $this->userToken = $this->getUserToken();
    $path = explode('/', $endpoint);

    switch ($path[0]) {
      case 'cost':
      case 'drivers':
      case 'eta':
      case 'ridetypes':
        $token = $this->clientToken;
        break;
      case 'profile':
      case 'rides':
        $token = $this->userToken;
        break;
    }

    return 'Authorization: '.
           ucwords($token['token_type']).' '.$token['access_token'];
  }

  public function getClientToken() {
    if ($this->clientToken &&
        !$this->tokenHasExpired($this->clientToken)) {
      return $this->clientToken;
    }

    $arr = [
      'grant_type' => 'client_credentials',
      'scope' => 'public',
    ];
    $json = json_encode($arr);
    $clientToken = $this->oauth($json);
    $clientToken['client_time'] = time();
    return $clientToken;
  }

  public function getUserToken($authCode=null) {
    if ($this->userToken && !$this->tokenHasExpired($this->userToken)) {
      return $this->userToken;
    }

    if ($refreshToken = $this->userToken['refresh_token']) {
      $arr = [
        'grant_type' => 'refresh_token',
        'refresh_token' => $refreshToken,
      ];
    } else {
      $arr = [
        'grant_type' => 'authorization_code',
        'code' => $authCode,
      ];
    }

    $json = json_encode($arr);
    $userToken = $this->oauth($json);
    $userToken['client_time'] = time();
    return $userToken;
  }

  public function get($endpoint, $queryString='', $version='v1') {
    $url = self::$baseUrl."/$version/$endpoint";
    $headers = [
      $this->getAuthHeader($endpoint),
    ];
    $result = $this->http('get', "$url?$queryString", $headers);
    return json_decode($result, true);
  }

  public function post($endpoint, $json=null, $version='v1') {
    $url = self::$baseUrl."/$version/$endpoint";
    $headers = [
      'Content-Type: application/json',
      $this->getAuthHeader($endpoint),
    ];
    $result = $this->http('post', $url, $headers, $json);
    return json_decode($result, true);
  }

  public function put($endpoint, $json=null, $version='v1') {
    $url = self::$baseUrl."/$version/$endpoint";
    $headers = [
      'Content-Type: application/json',
      $this->getAuthHeader($endpoint),
    ];
    $result = $this->http('put', $url, $headers, $json);
    return json_decode($result, true);
  }

  private function oauth($json) {
    $url = self::$baseUrl.'/oauth/token';
    $headers = [
      'Content-Type: application/json',
      'Authorization: Basic '.base64_encode("$this->clientId:$this->clientSecret"),
    ];
    $result = $this->http('post', $url, $headers, $json);
    return json_decode($result, true);
  }

  private function tokenHasExpired($token) {
    return time() > $token['client_time'] +
                    $token['expires_in'] -
                    self::$tokenRefreshOffset;
  }
}
